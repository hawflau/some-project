# Some project

## Run using `docker-compose`:
1. In `docker-compose.yml`, replace `some_google_api_key` with your Google Maps api key. To obtain one, refer to [here](https://developers.google.com/maps/documentation/directions/get-api-key)
```
....
  worker:
    build: .
    environment:
      - NODE_ENV=production
      - PORT=3000
      - GOOGLE_MAP_API_KEY=some_google_api_key
    command: npm run worker
....
```
2. In the project root folder, run:
```
docker-compose up
# or run in background
docker-compose up -d 
```
3. To exit, press `Ctrl+C` if you used `docker-compose up`.
If you run `docker-compose up -d`, run:
```
docker-compose down
```

## Explanation
### Engineering
There are two major components in this project:
* __`server`__ - a web server with two endpoints:
  * __POST__ `/route`
  * __GET__ `/route/:token`
* __`worker`__ - a worker script to process jobs, where jobs are created through the above POST endpoint

There are two dependent systems:
* a PostgreSQL database - to store search job and search result
* a Redis server - to serve a queue for `server` to post jobs and `worker` to pick up jobs

### Shortest Route Problem
Given one location as origin, and n locations, what is the shortest path if a driver starts from the origin and has to visits each of the n locations? (aka Travelling Salesman Problem)

#### Take 1
If number of locations is small, using brute force is a good idea. Find the distance between every two of the (n + 1) locations, then calculate total distance of each permutation to find the shortest path.
