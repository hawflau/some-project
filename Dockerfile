FROM node:8

RUN mkdir -p /usr/src/backend-challenge-leolau
WORKDIR /usr/src/backend-challenge-leolau

COPY package.json /usr/src/backend-challenge-leolau
RUN npm install

COPY . /usr/src/backend-challenge-leolau
