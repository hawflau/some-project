const request = require('supertest');
const expect = require('expect.js');
const db = require('../../src/db');
const app = require('../../src/app');
const Route = require('../../src/route');

const { knex, initDb } = db;

describe('routes', () => {
  before((done) => {
    initDb().then(done);
  });

  after((done) => {
    knex
      .schema
      .dropTable('routes')
      .then(() => knex.schema.dropTable('results'))
      .then(() => done());
  });

  describe('GET /health', () => {
    it('respond with json', (done) => {
      request(app)
        .get('/health')
        .set('Accept', 'application/json')
        .expect(200)
        .end((err, res) => {
          if (err) return done(err);
          return done();
        });
    });
  });

  describe('POST /route', () => {
    it('respond with error when input is missing', (done) => {
      request(app)
        .post('/route')
        .set('Accept', 'application/json')
        .expect(200, {
          error: 'Input is not an array',
        }, done);
    });

    it('respond with error when input is not an array', (done) => {
      request(app)
        .post('/route')
        .send({
          something: 'something',
        })
        .set('Accept', 'application/json')
        .expect(200, {
          error: 'Input is not an array',
        }, done);
    });

    it('respond with error when input is an array of length < 2', (done) => {
      request(app)
        .post('/route')
        .send([])
        .set('Accept', 'application/json')
        .expect(200, {
          error: 'Input should be an array of length >= 2',
        }, done);
    });

    it('respond with error when input contains invalid coordinates', (done) => {
      request(app)
        .post('/route')
        .send([['12.000', 'asbb'], ['122.000', '123.000']])
        .set('Accept', 'application/json')
        .expect(200, {
          error: 'Input contains invalid coordinates',
        }, done);
    });

    it('respond with token', (done) => {
      request(app)
        .post('/route')
        .send([
          ['22.372081', '114.107877'],
          ['22.284419', '114.159510'],
          ['22.326442', '114.167811'],
        ])
        .set('Accept', 'application/json')
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          const pattern = /[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/;
          expect(res.body).to.have.key('token');
          expect(res.body.token).to.match(pattern);
          return done();
        });
    });
  });

  describe('GET /route/:token', () => {
    let route;

    before((done) => {
      route = new Route(null, [
        ['22.372081', '114.107877'],
        ['22.284419', '114.159510'],
        ['22.326442', '114.167811'],
      ]);
      route
        .save()
        .then(result => null)
        .then(done);
    });

    it('respond with error when token is not uuid', (done) => {
      request(app)
        .get('/route/some-token')
        .set('Accept', 'application/json')
        .expect(200, {
          status: 'failure',
          error: 'Token is invalid',
        }, done);
    });

    it('respond with error for an unknown token', (done) => {
      request(app)
        .get('/route/9d3503e0-7236-4e47-a62f-8b01b5646c16')
        .set('Accept', 'application/json')
        .expect(200, {
          status: 'failure',
          error: 'token not found',
        }, done);
    });

    it('respond for a valid and created token', (done) => {
      request(app)
        .get(`/route/${route.token}`)
        .set('Accept', 'application/json')
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          console.log(res.body);
          return done();
        });
    });
  });
});
