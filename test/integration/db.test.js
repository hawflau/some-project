const db = require('../../src/db');
const Route = require('../../src/route');
const expect = require('expect.js');

const { knex, initDb } = db;

describe('db', () => {
  before((done) => {
    initDb().then(done);
  });

  after((done) => {
    knex
      .schema
      .dropTable('routes')
      .then(() => knex.schema.dropTable('results'))
      .then(() => done());
  });

  it('create routes table', (done) => {
    knex
      .schema
      .hasTable('routes')
      .then((exists) => {
        expect(exists).to.eql(true);
        done();
      });
  });

  it('create results table', (done) => {
    knex
      .schema
      .hasTable('results')
      .then((exists) => {
        expect(exists).to.eql(true);
        done();
      });
  });

  describe('Route', () => {
    let route;

    beforeEach((done) => {
      route = new Route(null, [
        ['22.372081', '114.107877'],
        ['22.284419', '114.159510'],
        ['22.326442', '114.167811'],
      ]);
      done();
    });

    it('have token as a uuid type', (done) => {
      const pattern = /[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/;
      expect(route.token).to.match(pattern);
      expect(route.locations).to.eql([
        ['22.372081', '114.107877'],
        ['22.284419', '114.159510'],
        ['22.326442', '114.167811'],
      ]);
      expect(route.status).to.eql('in progress');
      done();
    });

    it('save() after creation', (done) => {
      route
        .save()
        .then(() => (
          knex('routes')
            .select()
            .where({ token: route.token })
        ))
        .then((rows) => {
          expect(rows.length).to.eql(1);
          expect(rows[0].token).to.eql(route.token);
          done();
        })
        .catch(err => done(err));
    });

    it('save() after update', (done) => {
      route
        .save()
        .then(() => {
          route.status = 'failure';
          return route.save();
        })
        .then(() => (
          knex('routes')
            .select()
            .where({ token: route.token })
        ))
        .then((rows) => {
          expect(rows.length).to.eql(1);
          expect(rows[0].token).to.eql(route.token);
          expect(rows[0].status).to.eql('failure');
          done();
        })
        .catch(err => done(err));
    });

    it('load()', (done) => {
      route
        .save()
        .then(() => route.load())
        .then(({ token, locations, status }) => {
          expect(token).to.eql(route.token);
          expect(locations).to.eql(route.locations);
          expect(status).to.eql(route.status);
          done();
        })
        .catch(err => done(err));
    });

    it('findDirections()', (done) => {
      route
        .save()
        .then(() => route.findDirections())
        .then((result) => {
          // console.log(JSON.stringify(result, null, 4));
          expect(result.path).to.be.an('array');
          expect(result.total_distance).to.be.a('number');
          expect(result.total_time).to.be.a('number');
          done();
        })
        .catch(err => done(err));
    });
  });
});
