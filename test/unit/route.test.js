const Route = require('../../src/route');
const expect = require('expect.js');

describe('Route.findDirections()', () => {
  it('respond with success', (done) => {
    const route = new Route(null, [
      ['22.372081', '114.107877'],
      ['22.284419', '114.159510'],
      ['22.326442', '114.167811'],
    ]);

    route
      .findDirections()
      .then((result) => {
        expect(result.status).to.eql('success');
        expect(result.total_distance).to.be.a('number');
        expect(result.total_time).to.be.a('number');
        done();
      });
  });

  it('respond with failure', (done) => {
    const route = new Route(null, [
      ['22.372081', '114.107877'],
      ['22.284419', '214.159510'],
      ['22.326442', '114.167811'],
    ]);

    route
      .findDirections()
      .then((result) => {
        expect(result.status).to.eql('failure');
        done();
      });
  });
});

describe('Route.getShortestPath()', () => {
  it('respond', (done) => {
    const route = new Route(null, [
      ['22.372081', '114.107877'],
      ['22.284419', '114.159510'],
      ['22.326442', '114.167811'],
    ]);

    route
      .getShortestPath()
      .then((path) => {
        expect(path.path).to.be.an('array');
        expect(path.totalDistance).to.be.a('number');
        expect(path.totalDuration).to.be.a('number');
        done();
      });
  });
});
