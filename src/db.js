const env = process.env.NODE_ENV || 'development';
const CONFIG = require(`../config.${env}.json`);
const Debug = require('debug');
const knex = require('knex')(CONFIG.db);

/*
 * creates 'routes' table
 */
function createRoutesTable() {
  return knex
    .schema
    .createTableIfNotExists('routes', (table) => {
      table.increments();
      table.timestamps();
      table.uuid('token');
      table.jsonb('locations');
      table.enu('status', [
        'in progress',
        'failure',
        'success',
      ]);
      table.text('error');
    });
}

/*
 * create 'results' table
 */
function createResultsTable() {
  return knex
    .schema
    .createTableIfNotExists('results', (table) => {
      table.increments();
      table.timestamps();
      table.uuid('token');
      table.jsonb('path');
      table.float('total_distance');
      table.float('total_time');
    });
}

/*
 * init db
 */
function initDb() {
  const debug = Debug('initDb()');
  return Promise
    .all([
      createRoutesTable(),
      createResultsTable(),
    ])
    .then((results) => {
      debug(results);
    });
}

module.exports = {
  knex,
  initDb,
};
