const { Router } = require('express');
const Debug = require('debug');
const Route = require('../route');
const queue = require('../queue');

const routeRouter = Router();

/*
 * checks if coordinates is valid
 * @param {Array} coordinates
 * @return {boolean}
 */
function checkCoordinates(coordinates) {
  const debug = Debug('checkCoordinates()');
  if (!Array.isArray(coordinates)) {
    return false;
  }

  if (coordinates.length !== 2) {
    return false;
  }

  // each coordinate should be a number as string
  const isBothStringNum = coordinates
    .every(coordinate => (
      (typeof coordinate === 'string') &&
      (!isNaN(+coordinate))
    ));

  if (!isBothStringNum) {
    return false;
  }

  return true;
}

// ======================Middlewares=========================
/*
 * validates if input is valid
 */
function validatePostInput(req, res, next) {
  const debug = Debug('validatePostInput()');
  const { body } = req;

  if (!Array.isArray(body)) {
    return res
      .status(200)
      .json({
        error: 'Input is not an array',
      })
      .end();
  }

  if (body.length < 2) {
    return res
      .status(200)
      .json({
        error: 'Input should be an array of length >= 2',
      })
      .end();
  }

  const allPointsValid = body.every(checkCoordinates);

  if (!allPointsValid) {
    return res
      .status(200)
      .json({
        error: 'Input contains invalid coordinates',
      })
      .end();
  }

  return next();
}

routeRouter.post('/', validatePostInput, (req, res) => {
  const debug = Debug('POST /route');
  const { body } = req;
  debug(body);
  const route = new Route(null, body);
  debug(route);

  const promise = new Promise((resolve, reject) => {
    queue
      .create('job', {
        token: route.token,
        locations: route.locations,
      })
      .attempts(3)
      .backoff({ type: 'exponential' })
      .save((err) => {
        if (err) {
          return reject(err);
        }
        return resolve();
      });
  });

  return promise
    .then(() => route.save())
    .then(() => (
      res
        .status(200)
        .json({
          token: route.token,
        })
        .end()
    ))
    .catch(err => (
      res
        .status(200)
        .json({
          error: err.message,
        })
        .end()
    ));
});

routeRouter.get('/:token', (req, res) => {
  const { token } = req.params;
  // assuming a valid token must be of form of uuid
  const pattern = /^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/;
  if (!pattern.test(token)) {
    return res
      .status(200)
      .json({
        status: 'failure',
        error: 'Token is invalid',
      })
      .end();
  }

  const route = new Route(token);
  return route
    .load()
    .then(result => (
      res
        .status(200)
        .json(result)
        .end()
    ))
    .catch(err => (
      res
        .status(200)
        .json({
          status: 'failure',
          error: err.message,
        })
        .end()
    ));
});

module.exports = routeRouter;
