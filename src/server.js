const Express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const debug = require('debug')('server');
const { initDb } = require('./db');
const routeRouter = require('./routers/route');

const env = process.env.NODE_ENV || 'development';
const app = Express();

function initServer() {
  initDb()
    .then(() => {
      app.use(bodyParser.urlencoded({ extended: true }));
      app.use(bodyParser.json());

      app.use(morgan('combined'));

      // just a health check route
      app.get('/health', (req, res) => (res
        .json({
          status: 'OK',
          message: 'server is running',
        })
        .end()
      ));

      // set path '/route' to use routeRouter
      app.use('/route', routeRouter);

      const server = app.listen(process.env.PORT || 5000, '0.0.0.0', (err) => {
        if (err) {
          console.log(`=> OMG!!! ${err}`);
        }
        const { port } = server.address();
        console.log(`=> ${env} server is running on port ${port}`);
      });
    })
    .catch((err) => {
      // retry
      console.log(err);
      console.log(err.code);
      if (err.code === 'ECONNREFUSED') {
        console.log('retry to connect to db in 10s...');
        setTimeout(() => initServer(), 10000);
      } else {
        console.log('db access denied, exiting...');
        process.exit();
      }
    });
}

initServer();

module.exports = app;
