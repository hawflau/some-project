const kue = require('kue');

const env = process.env.NODE_ENV || 'development';
const CONFIG = require(`../config.${env}.json`);

const queue = kue.createQueue(CONFIG.kue);

module.exports = queue;
