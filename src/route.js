const { knex } = require('./db');
const uuidv4 = require('uuid/v4');
const googleMapsClient = require('@google/maps').createClient({
  key: process.env.GOOGLE_MAP_API_KEY || '',
  Promise: Promise,
});

/*
 * generates route's summary
 * @param {object} route
 * @return {array.<LatLng>} path
 * @return {number} total_distance
 * @return {number} total_time
 */
function summarizeRoute(route) {
  // https://developers.google.com/maps/documentation/directions/intro#Routes
  const { legs } = route;
  return legs.reduce(({ total_distance, total_time }, leg) => ({
    total_distance: leg.distance.value + total_distance,
    total_time: leg.duration.value + total_time,
  }), {
    total_distance: 0,
    total_time: 0,
  });
}

class Route {
  constructor(token, locations) {
    this.table = 'routes';
    this.resultTable = 'results';
    this.save = this.save.bind(this);
    this.load = this.load.bind(this);
    this.findDirections = this.findDirections.bind(this);

    if (!token && locations) {
      this.token = uuidv4();
      this.locations = locations;
      this.status = 'in progress';
    } else if (!token && !locations) {
      throw new Error('You must supply token or locations.');
    } else {
      this.token = token;
    }
    this.result = null;
  }

  save() {
    return knex(this.table)
      .select()
      .where({ token: this.token })
      .then((rows) => {
        const now = new Date();
        if (rows.length === 0) {
          return knex(this.table)
            .insert({
              created_at: now,
              updated_at: now,
              token: this.token,
              locations: JSON.stringify(this.locations),
              status: this.status,
            });
        }
        if (rows.length === 1) {
          const updateBody = {
            updated_at: now,
            status: this.status,
          };
          if (this.error) {
            updateBody.error = this.error;
          }

          return knex(this.table)
            .where({ token: this.token })
            .update(updateBody);
        }
        throw new Error('token not unique');
      })
      .then(() => {
        const now = new Date();
        if (this.status === 'success' && this.result) {
          return knex(this.resultTable)
            .insert({
              created_at: now,
              updated_at: now,
              token: this.token,
              path: JSON.stringify(this.result.path),
              total_distance: this.result.total_distance,
              total_time: this.result.total_time,
            });
        }
        return true;
      })
      .then(() => true);
  }

  load() {
    return knex(this.table)
      .select()
      .where({ token: this.token })
      .then((rows) => {
        if (rows.length === 0) {
          throw new Error('token not found');
        }
        if (rows.length === 1) {
          const { status, error } = rows[0];
          if (status === 'in progress') {
            return {
              status,
            };
          } else if (status === 'failure') {
            return {
              status,
              error,
            };
          } else if (status === 'success') {
            return knex(this.resultTable)
              .select()
              .where({ token: this.token })
              .then(rws => ({
                status,
                path: rws[0].path,
                total_distance: rws[0].total_distance,
                total_time: rws[0].total_time,
              }));
          }
          throw new Error('unknown status');
        }
        throw new Error('token not unique');
      });
  }

  findDirections() {
    if (!this.locations) {
      throw new Error('locations are not defined yet. Please use load()');
    }
    const origin = this.locations.slice(0).pop();
    const destination = this.locations.slice(-1).pop();
    const waypoints = this.locations.slice(1, -1);
    // populate query, reference:
    // https://developers.google.com/maps/documentation/directions/intro
    // https://googlemaps.github.io/google-maps-services-js/docs/GoogleMapsClient.html
    const query = {
      origin,
      destination,
      mode: 'driving',
      alternatives: true,
      units: 'metric',
    };
    if (waypoints.length > 0) {
      query.waypoints = waypoints;
    }
    return googleMapsClient
      .directions(query)
      .asPromise()
      .then((response) => {
        const { routes, status } = response.json;
        if (status !== 'OK') {
          return {
            status: 'failure',
            error: status,
          };
        }
        // routes
        // if there are more than 1 route, return the one with shortest distance
        const routeSummaries = routes.map(summarizeRoute);
        const shortestRoute = routeSummaries
          .sort((a, b) => {
            if (a.total_distance < b.total_distance) {
              return -1;
            }
            return 1;
          })[0];
        shortestRoute.path = this.locations;
        shortestRoute.status = 'success';
        return shortestRoute;
      })
      .catch((err) => {
        const { status } = err.json;
        return {
          status: 'failure',
          error: status,
        };
      });
  }

  getShortestPath() {
    const locations = this.locations.slice();

    function getDistanceMatrix() {
      const query = {
        origins: locations.slice(0),
        destinations: locations.slice(1),
        mode: 'driving',
        units: 'metric',
      };

      return googleMapsClient
        .distanceMatrix(query)
        .asPromise()
        .then((response) => {
          const { status, rows } = response.json;
          if (status !== 'OK') {
            throw new Error(status);
          }
          return rows;
        });
    }

    function getPermutations() {
      const permutations = [];
      const locIndexes = locations
        .slice(1)
        .map((l, i) => i + 1);

      function permute(arr, memo) {
        let cur;
        memo = memo || [];

        for (let i = 0; i < arr.length; i++) {
          cur = arr.splice(i, 1);
          if (arr.length === 0) {
            permutations.push(memo.concat(cur));
          }
          permute(arr.slice(), memo.concat(cur));
          arr.splice(i, 0, cur[0]);
        }

        return permutations;
      }

      return permute(locIndexes);
    }

    function calculateDistanceAndTime(permutation, matrix) {
      const reduced = permutation.reduce((acc, locIndex) => {
        const { prevStop, distance, duration } = acc;
        const cell = matrix[prevStop].elements[locIndex - 1];
        return {
          distance: distance + cell.distance.value,
          duration: duration + cell.duration.value,
          prevStop: locIndex,
        };
      }, {
        prevStop: 0,
        distance: 0,
        duration: 0,
      });

      return reduced;
    }

    if (!this.locations) {
      throw new Error('locations are not defined yet. Please use load()');
    }
    const permutations = getPermutations(locations);

    return getDistanceMatrix()
      .then((matrix) => {
        const shortest = permutations
          .map((p) => {
            const distAndTime = calculateDistanceAndTime(p, matrix);
            const path = locations.slice(0, 1).concat(p.map(i => locations[i]));
            return {
              path,
              total_distance: distAndTime.distance,
              total_time: distAndTime.duration,
            };
          })
          .sort((a, b) => {
            if (a.totalDistance < b.totalDistance) {
              return -1;
            }
            return 1;
          })[0];
        shortest.status = 'success';
        return shortest;
      })
      .catch(err => ({
        status: 'failure',
        error: err.message,
      }));
  }
}

module.exports = Route;
