const Debug = require('debug');
const Route = require('./route');
const queue = require('./queue');

queue
  .process('job', (job, done) => {
    const { token, locations } = job.data;
    const route = new Route(token);
    route.locations = locations;
    return route
      .getShortestPath()
      .then((result) => {
        const { status } = result;

        if (status === 'failure') {
          route.status = status;
          route.error = result.error;
        } else if (status === 'success') {
          route.status = status;
          route.result = result;
        } else {
          route.status = 'failure';
          route.error = 'Unknown error';
        }

        return route.save();
      })
      .then(() => done())
      .catch(err => {
        done(err);
      });
  });
